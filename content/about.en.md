---
title: "About me"
---
I'm Simone Dotto, a human born at the end of the last millenium. By chance I got interested in computers, and that enabled me to get into free
software, Linux and all that cool stuff. 

I started this blog with the intention of documenting my public contributions to FOSS projects, in the hope they might be useful to someone else.
It is also a way to be able to ask for donations by relying less on trust, making my contribution more transparent and obvious.

I appreciate feedback or suggestions: <a href="mailto:simone@dottoo.eu">email</a>.

All the contents are released under [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/), except where otherwise stated.
