---
title: Chi sono
slug: chi-sono
---
Mi chiamo Simone Dotto, sono nato a fine II millennio e sono un essere umano. In questa prima fase della mia vita il caso ha voluto che mi interessassi all'informatica, e ciò mi ha poi portato a conoscere il mondo del software libero, Linux e tutto ciò che ne consegue.

Il blog nasce con l'intento di documentare in particolare i miei contributi a vari progetti FLOSS, nella speranza che possano servire a qualcuno, o quantomeno ispirarlo. È anche un modo per poter chiedere donazioni in modo meno sfacciato, rendendo più trasparente ed evidente il mio contributo. 

Per commenti o proposte anche lavorative: <a href="mailto:simone@dottoo.eu">email</a>.

Tutti i contenuti del sito sono rilasciati con licenza [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/), salvo eccezioni riportate.
