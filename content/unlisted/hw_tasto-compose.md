+++
title = "Cos'è il tasto compose e come impostarlo in Xorg"
date = 2020-02-15
etichette = [ "tastiera", "xorg"]
+++

In questo articolo vedremo cos'è il **tasto compose**, come impostarlo in **Xorg** e come usarlo per inserire praticamente ogni carattere immaginabile in modo intuitivo.

## Cos’è il tasto compose

Il compose è un tasto che, quando premuto, attiva una modalità di inserimento particolare usata per inserire un carattere non presente sulla tastiera tramite una sequenza mnemonica di altri caratteri.

Detto così sembra molto complicato, ma vediamo subito un esempio. Per inserire il carattere spagnolo **ñ** mi basta premere in sequenza (non contemporaneamente) i tasti `compose`+`n`+`~`.

Nonostante la logica sia come quella appena mostrata, a volte non si riesce a trovare come si scrive un determinato carattere solo a buon senso. Una buona lista che può aiutarvi in questi casi la trovate su [fsymbols](https://fsymbols.com/keyboard/linux/compose/).

Ora mi direte: sulla tastiera non vedo alcun tasto compose. Avete perfettamente ragione, infatti a quanto pare era presente in origine solo in workstation UNIX, invece Windows e MacOS, che non lo supportano in modo nativo hanno vinto la guerra dei sistemi operativi e quindi è caduto nel dimenticatoio.

{{< figure src="/images/compose-key.jpg" caption="Tasto compose su una tastiera Sun Type 5 and 5c. [Compose key on Sun Type 5c keyboard](https://commons.wikimedia.org/wiki/File:Compose_key_on_Sun_Type_5c_keyboard.jpg), Felix the Cassowary, [CC BY-SA](https://creativecommons.org/licenses/by-sa/3.0/deed.en)" >}}

## Come impostarlo

In **Xorg** il tasto compose è preimpostato come la combo Shift+Alt Gr (in questa sequenza, scambiati sono un altro tasto), che lo rende abbastanza scomodo.

Per scoprire quali sono i tasti disponibili da impostare basta eseguire il comando:

```sh
$ grep "compose:" /usr/share/X11/xkb/rules/base.lst
```

Un esempio di output sul mio sistema è il seguente:

```
  compose:ralt         Right Alt
  compose:lwin         Left Win
  compose:lwin-altgr   3rd level of Left Win
  compose:rwin         Right Win
  compose:rwin-altgr   3rd level of Right Win
  compose:menu         Menu
  compose:menu-altgr   3rd level of Menu
  compose:lctrl        Left Ctrl
  compose:lctrl-altgr  3rd level of Left Ctrl
  compose:rctrl        Right Ctrl
  compose:rctrl-altgr  3rd level of Right Ctrl
  compose:caps         Caps Lock
  compose:caps-altgr   3rd level of Caps Lock
  compose:102          &lt;Less/Greater&gt;
  compose:102-altgr    3rd level of &lt;Less/Greater&gt;
  compose:paus         Pause
  compose:prsc         PrtSc
  compose:sclk         Scroll Lock
```

A questo per impostare il tasto prescelto basta eseguire il seguente comando (ad esempio con **Caps Lock**):

```sh
$ setxkbmap -option compose:caps
```
Questa configurazione è buona perchè Caps Lock è un tasto davvero grande in una posizione comoda e non viene usato spesso.

Una buona alternativa può essere Alt Gr, che a discapito di una minore comodità permette di mantenere il blocco delle maiuscole per le rare volte che serve. Ultimamente sono passato a questa configurazione.

Importante notare che l'impostazione non è persistente, quindi conviene aggiungere il comando al file `.xinitrc` in modo che sia eseguito all’avvio di Xorg.
