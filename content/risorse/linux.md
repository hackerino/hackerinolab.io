---
title: "Linux"
date: 2022-05-27
progetti: ["RORL"]
---

## Linux per sistemi integrati
:gb: [Embedded Linux Quick Start Guide v1.5](https://archive.org/details/embedded-linux-quick-start-1.5)

## Device driver
:gb: :book: [Linux Device Drivers](https://lwn.net/Kernel/LDD3/)

