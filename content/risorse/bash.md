---
title: "Bash"
date: 2022-05-28
progetti: ["RORL"]
---

## Documentazione
* :gb: [Bash Reference Manual](https://www.gnu.org/software/bash/manual/bash.html)

## Guide
* :gb: [Linux Bash Shell Scripting Tutorial ](https://bash.cyberciti.biz/guide/Main_Page)
