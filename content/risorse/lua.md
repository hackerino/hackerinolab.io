---
title: "Lua"
date: 2022-05-28
progetti: ["RORL"]
---

## Guida
* :gb: :book: [Programming in Lua (first edition)](http://www.lua.org/pil/contents.html)

## Panoramica
* :gb: [Learn Lua in Y Minutes](https://learnxinyminutes.com/docs/lua/)
